package com.example.springbootgitlab.springbootgitlab.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

	@GetMapping
	public ResponseEntity<String> getTestData(){
		System.out.println("adding logs for test");
		return new ResponseEntity<>("Test container working fine", HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<String> getUserById(@PathVariable("id") Long id){
		System.out.println("adding getUserById");
		return new ResponseEntity<>("ID that you passed"+id, HttpStatus.OK);
	}

}
