FROM openjdk:13-alpine
Volume /tmp
ADD /target/*.jar springboot-gitlab-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/springboot-gitlab-0.0.1-SNAPSHOT.jar"]